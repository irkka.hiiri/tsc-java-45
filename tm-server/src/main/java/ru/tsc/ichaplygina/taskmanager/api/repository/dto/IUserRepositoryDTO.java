package ru.tsc.ichaplygina.taskmanager.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.UserDTO;

import java.util.List;

public interface IUserRepositoryDTO extends IAbstractRepositoryDTO<UserDTO> {

    void clear();

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @Nullable
    UserDTO findById(@NotNull String id);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    String findIdByEmail(@NotNull String login);

    @Nullable
    String findIdByLogin(@NotNull String login);

    long getSize();

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);

}
