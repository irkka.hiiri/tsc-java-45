package ru.tsc.ichaplygina.taskmanager.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.UserDTO;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

public interface IUserServiceDTO extends IAbstractServiceDTO<UserDTO> {

    void add(@NotNull String login, @NotNull String password, @NotNull String email, @NotNull Role role,
             @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

    void add(@NotNull UserDTO user);

    void clear();

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable UserDTO findByLoginForAuthorization(@NotNull String login);

    boolean isPrivilegedUser(@NotNull String userId);

    boolean lockById(@NotNull String id);

    boolean lockByLogin(@NotNull String login);

    @Nullable
    UserDTO removeByLogin(@NotNull String login);

    void setPassword(@NotNull String login, @NotNull String password);

    void setRole(@NotNull String login, @NotNull Role role);

    boolean unlockById(@NotNull String id);

    boolean unlockByLogin(@NotNull String login);

    @Nullable
    UserDTO updateById(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull String email,
                       @NotNull Role role, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

    @Nullable
    UserDTO updateByLogin(@NotNull String login, @NotNull String password, @NotNull String email,
                          @NotNull Role role, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

}
