package ru.tsc.ichaplygina.taskmanager.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractBusinessEntityRepository<Task> {

    void clear();

    void clearForUser(@NotNull String userid);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByProjectId(@NotNull String projectId);

    @NotNull
    List<Task> findAllByProjectIdForUser(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<Task> findAllForUser(@NotNull String userId);

    @Nullable
    Task findById(@NotNull String id);

    @Nullable
    Task findByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findByIndex(int index);

    @Nullable
    Task findByIndexForUser(@NotNull String userId, int index);

    @Nullable
    Task findByName(@NotNull String name);

    @Nullable
    Task findByNameForUser(@NotNull String userId, @NotNull String name);

    @Nullable
    Task findTaskInProject(@NotNull String taskId, @NotNull String projectId);

    @Nullable
    Task findTaskInProjectForUser(@NotNull String userId,
                                  @NotNull String taskId,
                                  @NotNull String projectId);

    @Nullable
    String getIdByIndex(int index);

    @Nullable
    String getIdByIndexForUser(@NotNull String userId, int index);

    @Nullable
    String getIdByName(@NotNull String name);

    @Nullable
    String getIdByNameForUser(@NotNull String userId, @NotNull String name);

    long getSize();

    long getSizeForUser(@NotNull String userId);

    void removeAllByProjectId(@NotNull String projectId);

    void removeById(@NotNull String id);

    void removeByIdForUser(@NotNull String userId, @NotNull String id);

    void removeByIndex(int index);

    void removeByIndexForUser(@NotNull String userId, int index);

    void removeByName(@NotNull String name);

    void removeByNameForUser(@NotNull String userId, @NotNull String name);

}
