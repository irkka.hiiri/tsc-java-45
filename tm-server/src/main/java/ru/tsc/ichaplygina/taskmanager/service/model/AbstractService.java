package ru.tsc.ichaplygina.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IAbstractService;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.List;

public abstract class AbstractService<E extends AbstractModel> implements IAbstractService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public abstract void add(@NotNull final E entity);

    @Override
    public abstract void addAll(@Nullable List<E> entities);

    @Override
    public abstract void clear();

    @NotNull
    @Override
    public abstract List<E> findAll();

    @Nullable
    @Override
    public abstract E findById(@NotNull final String id);

    @Override
    public abstract long getSize();

    @Override
    public abstract boolean isEmpty();

    @Nullable
    @Override
    public abstract E removeById(@NotNull final String id);

}
