package ru.tsc.ichaplygina.taskmanager.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Session;

import java.util.List;

public interface ISessionRepository extends IAbstractRepository<Session> {

    void clear();

    @NotNull
    List<Session> findAll();

    @Nullable
    Session findById(@NotNull String id);

    long getSize();

    void removeById(@NotNull String id);

}
