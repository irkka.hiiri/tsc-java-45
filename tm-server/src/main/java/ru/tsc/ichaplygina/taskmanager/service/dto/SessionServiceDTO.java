package ru.tsc.ichaplygina.taskmanager.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.ISessionRepositoryDTO;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.ISessionServiceDTO;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IUserServiceDTO;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;
import ru.tsc.ichaplygina.taskmanager.dto.UserDTO;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.SessionNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedException;
import ru.tsc.ichaplygina.taskmanager.repository.dto.SessionRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.SignatureUtil.sign;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class SessionServiceDTO extends AbstractServiceDTO<SessionDTO> implements ISessionServiceDTO {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserServiceDTO userService;

    public SessionServiceDTO(@NotNull final IConnectionService connectionService,
                             @NotNull final IPropertyService propertyService,
                             @NotNull final IUserServiceDTO userService) {
        super(connectionService);
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final SessionDTO session) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = new SessionRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@Nullable List<SessionDTO> sessionList) {
        if (sessionList == null) return;
        for (final SessionDTO session : sessionList) add(session);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = new SessionRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public final void closeSession(@NotNull final SessionDTO session) {
        removeById(session.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = new SessionRepositoryDTO(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = new SessionRepositoryDTO(entityManager);
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = new SessionRepositoryDTO(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @NotNull
    @Override
    @SneakyThrows
    public final SessionDTO openSession(@NotNull final String login, @NotNull final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final UserDTO user = Optional.ofNullable(userService.findByLoginForAuthorization(login)).orElseThrow(IncorrectCredentialsException::new);
            if (!user.getPasswordHash().equals(salt(password, propertyService)))
                throw new IncorrectCredentialsException();
            if (user.isLocked()) throw new AccessDeniedException();
            @NotNull final SessionDTO session = new SessionDTO(user.getId());
            sign(session, propertyService);
            @NotNull final ISessionRepositoryDTO repository = new SessionRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.add(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = new SessionRepositoryDTO(entityManager);
            @NotNull final SessionDTO session = Optional.ofNullable(repository.findById(id)).orElseThrow(SessionNotFoundException::new);
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final void validatePrivileges(@NotNull final String userId) {
        if (!userService.isPrivilegedUser(userId))
            throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public final void validateSession(@Nullable final SessionDTO session) {
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        @Nullable final String signature = session.getSignature();
        Optional.ofNullable(signature).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(findById(session.getId())).orElseThrow(AccessDeniedException::new);
        @NotNull final SessionDTO sessionClone = session.clone();
        if (!signature.equals(sign(sessionClone, propertyService).getSignature()))
            throw new AccessDeniedException();
    }

}
