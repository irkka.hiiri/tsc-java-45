package ru.tsc.ichaplygina.taskmanager.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

public interface IProjectRepository extends IAbstractBusinessEntityRepository<Project> {

    void clear();

    void clearForUser(@NotNull String userid);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAllForUser(@NotNull String userId);

    @Nullable
    Project findById(@NotNull String id);

    @Nullable
    Project findByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findByIndex(int index);

    @Nullable
    Project findByIndexForUser(@NotNull String userId, int index);

    @Nullable
    Project findByName(@NotNull String name);

    @Nullable
    Project findByNameForUser(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByIndex(int index);

    @Nullable
    String getIdByIndexForUser(@NotNull String userId, int index);

    @Nullable
    String getIdByName(@NotNull String name);

    @Nullable
    String getIdByNameForUser(@NotNull String userId, @NotNull String name);

    long getSize();

    long getSizeForUser(@NotNull String userId);

    void removeById(@NotNull String id);

    void removeByIdForUser(@NotNull String userId, @NotNull String id);

    void removeByIndex(int index);

    void removeByIndexForUser(@NotNull String userId, int index);

    void removeByName(@NotNull String name);

    void removeByNameForUser(@NotNull String userId, @NotNull String name);

}
