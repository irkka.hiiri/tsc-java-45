package ru.tsc.ichaplygina.taskmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.entity.IWBS;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_task")
public final class TaskDTO extends AbstractBusinessEntityDTO implements IWBS {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public TaskDTO(@NotNull final String name, @NotNull final String userId) {
        super(name, userId);
    }

    public TaskDTO(@NotNull final String name, @Nullable final String description, @NotNull final String userId) {
        super(name, description, userId);
    }

}
