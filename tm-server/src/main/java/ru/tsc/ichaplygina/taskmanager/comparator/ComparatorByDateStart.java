package ru.tsc.ichaplygina.taskmanager.comparator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.entity.IHasDateStart;

import java.util.Comparator;

public final class ComparatorByDateStart implements Comparator<IHasDateStart> {

    @NotNull
    private static final ComparatorByDateStart INSTANCE = new ComparatorByDateStart();

    private ComparatorByDateStart() {
    }

    @NotNull
    public static ComparatorByDateStart getInstance() {
        return INSTANCE;
    }

    @Override
    public final int compare(@Nullable final IHasDateStart o1, @Nullable final IHasDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateStart() == null && o2.getDateStart() == null) return 0;
        if (o1.getDateStart() == null) return -1;
        if (o2.getDateStart() == null) return 1;
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
