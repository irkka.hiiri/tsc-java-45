package ru.tsc.ichaplygina.taskmanager.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class UserExistsWithLoginException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "User already exists with login: ";

    public UserExistsWithLoginException(final String login) {
        super(MESSAGE + login);
    }

}
