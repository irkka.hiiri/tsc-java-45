package ru.tsc.ichaplygina.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface IFileScannerProperty {

    @NotNull Integer getFileScannerFrequency();

    @NotNull String getFileScannerPath();

}
