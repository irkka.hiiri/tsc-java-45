package ru.tsc.ichaplygina.taskmanager.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.EndpointLocator;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.endpoint.AdminEndpoint;
import ru.tsc.ichaplygina.taskmanager.endpoint.Session;
import ru.tsc.ichaplygina.taskmanager.endpoint.SessionEndpoint;
import ru.tsc.ichaplygina.taskmanager.exception.other.EndpointLocatorNotFoundException;

import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public abstract class AbstractCommand {

    @NotNull
    protected static final String ID_INPUT = "Please enter id: ";

    @NotNull
    protected static final String INDEX_INPUT = "Please enter index: ";

    @NotNull
    protected static final String NAME_INPUT = "Please enter name: ";

    @NotNull
    protected static final String DESCRIPTION_INPUT = "Please enter description: ";

    @Nullable
    protected EndpointLocator endpointLocator;

    public abstract void execute();

    @Nullable
    public abstract String getArgument();

    @NotNull
    protected ICommandService getCommandService() {
        if (!Optional.ofNullable(endpointLocator).isPresent()) throw new EndpointLocatorNotFoundException();
        return endpointLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        if (!Optional.ofNullable(endpointLocator).isPresent()) throw new EndpointLocatorNotFoundException();
        return endpointLocator.getPropertyService();
    }

    @Nullable
    protected Session getSession() {
        if (!Optional.ofNullable(endpointLocator).isPresent()) throw new EndpointLocatorNotFoundException();
        return endpointLocator.getSession();
    }

    protected void setSession(@Nullable final Session session) {
        if (!Optional.ofNullable(endpointLocator).isPresent()) throw new EndpointLocatorNotFoundException();
        endpointLocator.setSession(session);
    }

    @NotNull
    public abstract String getCommand();

    @NotNull
    public abstract String getDescription();

    @NotNull
    protected AdminEndpoint getAdminEndpoint() {
        if (!Optional.ofNullable(endpointLocator).isPresent()) throw new EndpointLocatorNotFoundException();
        return endpointLocator.getAdminEndpoint();
    }

    @NotNull
    protected SessionEndpoint getSessionEndpoint() {
        if (!Optional.ofNullable(endpointLocator).isPresent()) throw new EndpointLocatorNotFoundException();
        return endpointLocator.getSessionEndpoint();
    }

    public void setEndpointLocator(@NotNull final EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

    @Override
    public String toString() {
        return (getCommand() + (isEmptyString(getArgument()) ? "" : " [" + getArgument() + "]") + " - " + getDescription() + ".");
    }

}
