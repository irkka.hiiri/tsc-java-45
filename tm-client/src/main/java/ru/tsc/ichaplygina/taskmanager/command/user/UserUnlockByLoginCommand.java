package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class UserUnlockByLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "unlock user by login";

    @NotNull
    public static final String DESCRIPTION = "unlock user by login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        getAdminEndpoint().unlockUserByLogin(getSession(), login);
    }

}
