package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.endpoint.Project;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.DELIMITER;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.SORT_HINT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "list projects";

    @NotNull
    public final static String DESCRIPTION = "show all projects";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String sortBy = readLine(SORT_HINT);
        @Nullable final List<Project> projectList = getProjectEndpoint().getProjectList(getSession(), sortBy);
        if (projectList == null) return;
        System.out.println("Id : Name : Description : Created : Status : Start Date : End Date : User Id");
        int index = 1;
        for (@NotNull final Project project : projectList) {
            System.out.println(index + DELIMITER +
                    project.getId() + DELIMITER +
                    project.getName() + DELIMITER +
                    project.getDescription() + DELIMITER +
                    project.getCreated() + DELIMITER +
                    project.getStatus() + DELIMITER +
                    project.getDateStart() + DELIMITER +
                    project.getDateFinish() + DELIMITER +
                    project.getUser().getId());
            index++;
        }
    }

}
